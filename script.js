function tabsDrop(event, tabName) {
    let i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tabs-title");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    event.currentTarget.className += " active";
  }
let testim = document.getElementById("testim"),
  testimDots = Array.prototype.slice.call(
    document.getElementById("testim-dots").children
  ),
  testimContent = Array.prototype.slice.call(
    document.getElementById("testim-content").children
  ),
  testimLeftArrow = document.getElementById("left-arrow"),
  testimRightArrow = document.getElementById("right-arrow"),
  testimSpeed = 4500,
  currentSlide = 0,
  currentActive = 0,
  testimTimer,
  touchStartPos,
  touchEndPos,
  touchPosDiff,
  ignoreTouch = 30;
window.onload = function () {
  function playSlide(slide) {
    for (let k = 0; k < testimDots.length; k++) {
      testimContent[k].classList.remove("active");
      testimContent[k].classList.remove("inactive");
      testimDots[k].classList.remove("active");
    }

    if (slide < 0) {
      slide = currentSlide = testimContent.length - 1;
    }

    if (slide > testimContent.length - 1) {
      slide = currentSlide = 0;
    }

    if (currentActive != currentSlide) {
      testimContent[currentActive].classList.add("inactive");
    }
    testimContent[slide].classList.add("active");
    testimDots[slide].classList.add("active");

    currentActive = currentSlide;

    clearTimeout(testimTimer);
    testimTimer = setTimeout(function () {
      playSlide((currentSlide += 1));
    }, testimSpeed);
  }

  testimLeftArrow.addEventListener("click", function () {
    playSlide((currentSlide -= 1));
  });

  testimRightArrow.addEventListener("click", function () {
    playSlide((currentSlide += 1));
  });

  for (let l = 0; l < testimDots.length; l++) {
    testimDots[l].addEventListener("click", function () {
      playSlide((currentSlide = testimDots.indexOf(this)));
    });
  }

  playSlide(currentSlide);

  document.addEventListener("keyup", function (e) {
    switch (e.keyCode) {
      case 37:
        testimLeftArrow.click();
        break;

      case 39:
        testimRightArrow.click();
        break;

      case 39:
        testimRightArrow.click();
        break;

      default:
        break;
    }
  });

  testim.addEventListener("touchstart", function (e) {
    touchStartPos = e.changedTouches[0].clientX;
  });

  testim.addEventListener("touchend", function (e) {
    touchEndPos = e.changedTouches[0].clientX;

    touchPosDiff = touchStartPos - touchEndPos;

    console.log(touchPosDiff);
    console.log(touchStartPos);
    console.log(touchEndPos);

    if (touchPosDiff > 0 + ignoreTouch) {
      testimLeftArrow.click();
    } else if (touchPosDiff < 0 - ignoreTouch) {
      testimRightArrow.click();
    } else {
      return;
    }
  });
};
let images = {
  "All": ['./img/graphic-design1.jpg', './img/Layer 34.png', './img/Layer 33.png', './img/Layer 32.png', './img/Layer 31.png', './img/Layer 30.png', './img/Layer 29.png', './img/Layer 28.png', './img/Layer 27.png', './img/Layer 26.png', './img/Layer 25.png', './img/Layer 24.png'],
  "Graphic Design": ['./img/graphic-design1.jpg', './img/Layer 34.png', './img/Layer 33.png'],
  "Web Design": ['./img/Layer 32.png', './img/Layer 31.png', './img/Layer 30.png'],
  "Landing Pages": ['./img/Layer 29.png', './img/Layer 28.png', './img/Layer 27.png'],
  "Wordpress": ['./img/Layer 26.png', './img/Layer 25.png', './img/Layer 24.png']
};
let loaderImages = {
  "LOAD MORE": ['./img/graphic-design2.jpg', './img/graphic-design3.jpg', './img/graphic-design4.jpg', './img/landing-page1.jpg', 
  './img/landing-page2.jpg', './img/landing-page3.jpg', './img/web-design1.jpg', './img/web-design2.jpg', './img/web-design3.jpg', 
  './img/wordpress1.jpg', './img/wordpress2.jpg', './img/wordpress3.jpg']
};
let setImages = new Object();
  setImages.getMenu = function() {
  let ul = '<ul class="secondTab">';
  for (let gictg in images) {
    ul += '<li class="tabsWork" onclick="setImages.getImages(\''+ gictg+ '\')">'+ gictg+ '</li>';
   }
   return ul+ '</ul>';
  }
setImages.getImages = function(gictg) {
  let nrim = images[gictg].length;
  let ul = '';
  for(let i=0; i<nrim; i++) {
  ul += '<div class="ulImg"><img class="loaderImg" src="'+ images[gictg][i]+ '"" /><div class="imgHover"><img class="hoverIcon" src="./img/hoverIcon.png"/><div class="hoverFirst">CREATIVE DESIGN</div><div class="hoverSecond">Web Design</div></div></div>';
  }
  document.getElementById('gimgs').innerHTML = ul;
 }
 document.getElementById('gimenu').innerHTML = setImages.getMenu();
 let loadImg = function() {
  images['All'] = [...images['All'], ...loaderImages['LOAD MORE']];
  images['Graphic Design'] = [...images['Graphic Design'], ...loaderImages['LOAD MORE'].slice(0, 3)];
  images['Web Design'] = [...images['Web Design'], ...loaderImages['LOAD MORE'].slice(6, 9)];
  images['Landing Pages'] = [...images['Landing Pages'], ...loaderImages['LOAD MORE'].slice(3, 6)];
  images['Wordpress'] = [...images['Wordpress'], ...loaderImages['LOAD MORE'].slice(9, 13)];
  setImages.getImages('All')
  document.getElementById('loadButton').style.display = 'none';
 }